#!/usr/bin/env python3

import sys

if __name__ == '__main__':
    for line in sys.stdin:
        buildings = list(map(int, line.strip().split()))
        sunrise = len(buildings)
        for i in range(len(buildings)):
            for j in range(i+1, len(buildings)):
                if buildings[i] <= buildings[j]:
                    sunrise -= 1
                    break
        print(sunrise)





