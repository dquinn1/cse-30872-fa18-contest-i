#!/usr/bin/env python3

import sys
import itertools

def generate_binary_strings(binary_nums, n, k, counts):
    for perm in itertools.product('01', repeat=n):
        perm = ''.join(list(map(str, perm)))
        for num in perm:
            counts[num] += 1
        if counts['1'] == k:
            print(perm)
        counts = {'0': 0, '1': 0}

if __name__ == '__main__':
    count = 0
    for line in sys.stdin:
        if count != 0:
            print('')
        n = int(line.strip().split()[0])
        k = int(line.strip().split()[1])
        counts = {'0': 0, '1': 0}
        generate_binary_strings('01', n, k, counts)
        count += 1
