#!/usr/bin/env python3

import sys

def KswapPermutation(nums, n, k): 
  
    pos = {} 
    for i in range(n): 
        pos[nums[i]] = i 
  
    for i in range(n): 
  
        if k == 0: 
            break
  
        if nums[i] == n-i: 
            continue
  
        # swap if not equal
        j = pos[n-i] 
        pos[nums[i]] = pos[n-i] 
        pos[n-i] = i 
        temp = nums[j]
        temp = nums[i]
        nums[i] = nums[j] 
        nums[j] = temp
  
        k -= 1



if __name__ == '__main__':
    while True:
        try:
            parameters = sys.stdin.readline().strip().split()
            n = int(parameters[0])
            k = int(parameters[1])
            nums = list(map(int,sys.stdin.readline().strip().split()))
        except ValueError:
            break
        except IndexError:
            break
        KswapPermutation(nums, n, k)
        print(' '.join(map(str,nums)))







