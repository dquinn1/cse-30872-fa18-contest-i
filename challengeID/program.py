#!/usr/bin/env python3

import sys

def optimal_gap(pots, n_flowers):
    first = pots[0]
    last = pots[-1]
    p = last - first + 1
    n_empty = p - n_flowers
    optimal = n_empty // (n_flowers - 1)
    found = False
    while not found:
        current = first
        n_planted = 1
        do_break = False
        while n_planted < n_flowers:
            current += optimal + 1
            while current not in pots:
                if current > last:
                    do_break = True
                    break
                current += 1
            if do_break:
                optimal -= 1
                break
            n_planted += 1
            if n_planted == n_flowers:
                found = True
                break
    return optimal




if __name__ == '__main__':
    while True:
        pots = []
        try:
            line = sys.stdin.readline().rstrip()
            n_pots = int(line.split()[0])
            n_flowers = int(line.split()[1])
        except ValueError:
            break
        except IndexError:
            break
        for i in range(n_pots):
            try:
                pots.append(int(sys.stdin.readline().strip()))
            except ValueError:
                break
        pots = sorted(pots)
        print(optimal_gap(pots, n_flowers))
