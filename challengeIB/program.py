#!/usr/bin/env python3

import sys

if __name__ == '__main__':
    for line in sys.stdin:
        mapping = {}
        line = line.split()
        first = line[0]
        last = line[1]
        notIso = False
        for count, letter in enumerate(first):
            if letter in mapping:
                if mapping[letter] != last[count]:
                    notIso = True
                    break
            mapping[letter] = last[count]
        if notIso:
            print('Not Isomorphic')
        else:
            print('Isomorphic')

