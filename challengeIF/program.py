#!/usr/bin/env python3

import sys
import itertools
import operator

def possible_scores(points, score_types):
    results = []
    for i in range(points):
        for combo in itertools.combinations_with_replacement(score_types, i):
            if sum(combo) == points:
                combo_string =' '.join(list(map(str,combo)))
                results.append(combo_string)
    results = sorted(results)
    n_ways = len(results)
    if n_ways == 1:
        print('There is {} way to achieve a score of {}:'.format(n_ways, points))
    else:
        print('There are {} ways to achieve a score of {}:'.format(n_ways, points))
    for solution in results:
        print(solution)


if __name__ == '__main__':
    score_types = [2, 3, 7]
    for line in sys.stdin:
        points = int(line.strip())
        possible_scores(points, score_types)
                
